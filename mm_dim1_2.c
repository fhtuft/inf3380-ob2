/*
 *Program that perform  matrix multiplication A*B=C
*/
#include <stdio.h>


#include "debug.h"
#include "util.h"
#include "common.h"

#include <mpi.h>
#include <stdlib.h>
#include <stdbool.h>

#define ROOT         0
#define PRINT_MATRIX 1

static void print_matrix(int num_rows, int num_cols, double **matrix,
			 char *string) {
  if(!PRINT_MATRIX) return;
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
 
  char filename[20];
  int i,j;
  snprintf(filename,19,"%s_rank%d.m",string,my_rank);
   FILE *f = fopen(filename, "w");
   if (f == NULL) {
       printf("Error opening file!\n");
       exit(1);
   }
   

  for(i=0; i<num_rows; i++) {
    fprintf(f,"%!!%d!!",i);
    for(j=0; j<num_cols; j++) {
      fprintf(f,"%f ",matrix[i][j]);
    }
    fprintf(f,":\n");
  }
  printf("printed %s\n",filename);
  fflush(f);
  fclose(f);

}



int main(int argc, char *argv[]) {

  int i,j,k;/* loop variabls */

  /* temporary matrix */
  //double **matrix;
  //int num_rows,num_cols;

  /* Some common stuff */
  int num_rows_global_a;
  int num_cols_global_a;
  int num_rows_global_b;
  int num_cols_global_b;
  int num_rows_global_c;
  int num_cols_global_c;

  /* matrix A */
  double **matrix_a;
  int num_rows_a, num_cols_a;
  /* matrix B */
  double **matrix_b;
  int num_rows_b, num_cols_b;
  /* matrix C */
  double **matrix_c;
  int num_rows_c, num_cols_c;

  /* MPI*/
  int my_rank, num_procs;
  //int mpi_error;

  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

  
  assert_arg_number(argc);
  
  #if 1
  /* Scatter matrix A */
  {
    double **matrix;
    int num_rows,num_cols;


    

    if(my_rank == ROOT) {
      read_matrix_binary(argv[1],&matrix, &num_rows, &num_cols);
      print_matrix(num_rows,num_cols,matrix,"matrixA");

    }
    

    MPI_Bcast(&num_rows,1,MPI_INT,ROOT,MPI_COMM_WORLD);
    MPI_Bcast(&num_cols,1,MPI_INT,ROOT,MPI_COMM_WORLD);
    
    
    num_rows_a = num_rows/num_procs +(num_rows%num_procs>my_rank);
    num_cols_a = num_cols;
    num_rows_c = num_rows_a;
    num_rows_global_a = num_rows;
    num_cols_global_a = num_cols;
    num_rows_global_c = num_rows_global_a;
    
    fprintf(stderr,"matrix A rank(%d) num_rows_a:%d num_cols_a:%d \n",my_rank,num_rows_a,num_cols_a);
    
    make_matrix(num_rows_a, num_cols_a,&matrix_a);
       

    int *sendcount      = (int *)safe_malloc(num_procs*sizeof(int));
    int *displs         = (int *)safe_malloc(num_procs*sizeof(int));
    int recvcount       = num_rows_a*num_cols_a;
    
    fprintf(stderr,"matrix A rank(%d) num_rows_a %d  recvcount %d\n",my_rank,num_rows_a,recvcount);

    int offset = 0;
    for (i=0; i<num_procs; i++) {
      sendcount[i] =(num_rows/num_procs +(num_rows%num_procs>i))*num_cols_a;
      displs[i] = offset;
      offset += sendcount[i];
      fprintf(stderr,"matrix A rank(%d) i:%d displ:%d sendcout:%d \n",my_rank,i,displs[i],sendcount[i]);
    }
   
    void *sendbuf = (my_rank == ROOT)? &(matrix[0][0]):NULL;

    MPI_Scatterv(sendbuf,sendcount,displs,
		 MPI_DOUBLE,&(matrix_a[0][0]),recvcount,
		 MPI_DOUBLE,
		 ROOT,MPI_COMM_WORLD);
    
    print_matrix(num_rows_a,num_cols_a,matrix_a,"matrix_A_S");
    

    free(sendcount);
    free(displs);

    if(my_rank == ROOT)
      free_matrix(matrix);
  
  }
#endif
  
  /* 
   * Scatter matrix B 
  */  
  #if 1
    {

      double **matrix;
      int num_rows,num_cols;
      

      if(my_rank == ROOT) {
	read_matrix_binary(argv[2],&matrix, &num_rows, &num_cols);
	print_matrix(num_rows,num_cols,matrix,"matrixB");
      }

      MPI_Bcast(&num_rows,1,MPI_INT,ROOT,MPI_COMM_WORLD);
      MPI_Bcast(&num_cols,1,MPI_INT,ROOT,MPI_COMM_WORLD);
  
    
      num_rows_b = (num_rows/num_procs) +(num_rows%num_procs>my_rank);
      num_cols_b = num_cols;
      num_cols_c = num_cols_b;
      num_cols_global_b = num_cols;
      num_rows_global_b = num_rows;
      num_cols_global_c = num_cols_global_b;

      make_matrix(num_rows_b, num_cols_b,&matrix_b);
      //matrix_b = (double **)malloc(num_rows_b*sizeof(double*));
      //matrix_b[0] = (double *)calloc(num_rows_b*num_cols_b,sizeof(double));
      //for(i =1; i<num_rows_b; i++)
      //matrix_b[i] = matrix_b[i-1] + num_cols_b;
      

      int *sendcount     = (int *)safe_malloc(num_procs*sizeof(int));
      int *displs        = (int *)safe_malloc(num_procs*sizeof(int));
      int recvcount      = num_rows_b*num_cols_b;
    
      fprintf(stderr,"matrix B rank(%d) num_rows_b %d  recvcount %d\n",my_rank,num_rows_b,recvcount);
      int offset = 0;
      for (i=0; i<num_procs; i++) {
	sendcount[i] =((num_rows/num_procs) +(num_rows%num_procs>i))*num_cols_b;	
	displs[i] = offset;
	offset += sendcount[i];
	fprintf(stderr,"matrix B rank(%d) i:%d displ:%d sendcout:%d \n",my_rank,i,displs[i],sendcount[i]);

      }
      fflush(stderr);
      
      void* sendbuf = (my_rank == ROOT)? &(matrix[0][0]):NULL;

      MPI_Scatterv(sendbuf,sendcount,displs,
		   MPI_DOUBLE,&(matrix_b[0][0]),recvcount,
		   MPI_DOUBLE,
		   ROOT,MPI_COMM_WORLD);
    
      print_matrix(num_rows_b,num_cols_b,matrix_b,"matrix_B_S");
      free(sendcount);
      free(displs);
    
      if(my_rank == ROOT)
	free_matrix(matrix);
  
    }
#endif


#if 1  

  /* 
   * Assert correct dimensions in the factors */
  //assert_product_dim(num_cols_a == num_rows_b); 
  /* Make matrix C */
  fprintf(stderr,"matrix C rank(%d) num_rows_c %d num_cols_c %d \n",my_rank,num_rows_c,num_cols_c);
  fflush(stderr);
  
  make_matrix(num_rows_c, num_cols_c,&matrix_c);
  
#endif
 
  
#if 1
  {

    int offset;
    int sendcount;
    int *recvcount;
    int *displs;
    double *send_array;
    double *b_col_array;
    recvcount = (int *)safe_malloc(num_procs*sizeof(int));
    displs    = (int *)safe_malloc(num_procs*sizeof(int));
    b_col_array = (double*)safe_malloc(num_cols_a*sizeof(double)); /* ikke helt bra med num_cols_a...*/
    send_array = (double*)safe_malloc(num_rows_b*sizeof(double));
    sendcount = num_rows_b;

    offset = 0;
    for(i =0; i<num_procs; i++) {
      recvcount[i] = ((num_rows_global_b/num_procs) + (num_rows_global_b%num_procs > i));
      displs[i]    = offset;
      offset      += recvcount[i];
      fprintf(stderr,"allgather rank(%d) i:%d displ:%d recvcount:%d \n",my_rank,i,displs[i],recvcount[i]);	
    }
    

    /* A*B=C */
    for(j = 0; j<num_cols_c; j++) {
  
      for(k=0; k<num_rows_b; k++) {
	send_array[k] = matrix_b[k][j];
      }
      MPI_Allgatherv(&(send_array[0]),sendcount,MPI_DOUBLE,
		     &(b_col_array[0]),&(recvcount[0]),&(displs[0])
		     ,MPI_DOUBLE, MPI_COMM_WORLD);
    
      for(i =0; i<num_rows_c ; i++) {
    
	/* Vector inner product */
	for(k = 0; k< num_cols_a; k++) {
	  matrix_c[i][j] += matrix_a[i][k]*b_col_array[k];
	}
      }
    }

    //free(b_col_array);
  }

#if 1
  {

   
    int i;
    int offset;
    double **matrix;
    int num_row, num_cols;
    int sendcount;
    int *recvcounts;
    int *displs;


    sendcount = num_rows_c*num_cols_c;
    fprintf(stderr,"gatherv rank(%d) sendcount %d \n",my_rank,sendcount);
    
    recvcounts = (int *)safe_malloc(num_procs*sizeof(int));
    displs    = (int *)safe_malloc(num_procs*sizeof(int));

    offset =0;
    for (i=0; i<num_procs; i++) {
	recvcounts[i] =((num_rows_global_c/num_procs) +(num_rows_global_c%num_procs>i))*num_cols_global_c;	
	displs[i] = offset;
	offset += recvcounts[i];
	fprintf(stderr,"gatherv rank(%d) i:%d displ:%d recvcounts:%d \n",my_rank,i,displs[i],recvcounts[i]);	
    }

    if(my_rank == ROOT) {
      make_matrix(num_rows_global_c,num_cols_global_c,&matrix);
    }
    void *recvbuf = (void *)(my_rank == ROOT)? &(matrix[0][0]):NULL;

    MPI_Gatherv(&(matrix_c[0][0]),sendcount, MPI_DOUBLE,
		recvbuf,recvcounts,displs,
                MPI_DOUBLE,ROOT, MPI_COMM_WORLD);

    
    if(my_rank == ROOT) {
      /* Write the product matrix to file */
      write_matrix_binary(argv[3],matrix,num_rows_global_c,num_cols_global_c);
    }
    
  }
#endif
  /* Free alloced memory */
  free_matrix(matrix_a);
  free_matrix(matrix_b);
  free_matrix(matrix_c);

  #endif
  MPI_Finalize();

  return 0;

}
