/*
 *Program that perform  matrix multiplication A*B=C
*/
#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <stdlib.h>
#include <stdbool.h>


#include "debug.h"
#include "util.h"
#include "common.h"


#define ROOT         0
#define PRINT_MATRIX 1

static void print_matrix(int num_rows, int num_cols, double **matrix,
			 char *string) {
  if(!PRINT_MATRIX) return;
  int my_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
 
  char filename[20];
  int i,j;
  snprintf(filename,19,"%s_rank%d.m",string,my_rank);
   FILE *f = fopen(filename, "w");
   if (f == NULL) {
       printf("Error opening file!\n");
       exit(1);
   }
   

  for(i=0; i<num_rows; i++) {
    fprintf(f,"%!!%d!!",i);
    for(j=0; j<num_cols; j++) {
      fprintf(f,"%f ",matrix[i][j]);
    }
    fprintf(f,":\n");
  }
  printf("printed %s\n",filename);
  fflush(f);
  fclose(f);

}



int main(int argc, char *argv[]) {

  int i,j,k;/* loop variabls */

  /* temporary matrix */
  //double **matrix;
  //int num_rows,num_cols;

  /* Some common stuff */
  int num_rows_global_a;
  int num_cols_global_a;
  int num_rows_global_b;
  int num_cols_global_b;
  int num_rows_global_c;
  int num_cols_global_c;

  /* matrix A */
  double **matrix_a;
  int num_rows_a, num_cols_a;
  /* matrix B */
  double **matrix_b;
  int num_rows_b, num_cols_b;
  /* matrix C */
  double **matrix_c;
  int num_rows_c, num_cols_c;

  /* MPI*/
  int my_rank, num_procs;
  int my_cart_rank;
  MPI_Comm comm_cart;
  MPI_Comm comm_cols;
  MPI_Comm comm_rows;
  int num_procs_per_dim;
  int dims[2],periods[2],my_coords[2];


  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
  
  assert_arg_number(argc);

  num_procs_per_dim = (int)sqrt(num_procs); 

  dims[0] = dims[1] = num_procs_per_dim;
  periods[0] =  periods[1] = 1;

  MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 1, &comm_cart);
  MPI_Comm_rank(comm_cart, &my_cart_rank);
  MPI_Cart_coords(comm_cart, my_cart_rank, 2, my_coords);

  MPI_Cart_sub(comm_cart, (int[]){0, 1}, &comm_rows);
  MPI_Cart_sub(comm_cart, (int[]){1, 0}, &comm_cols);

  fprintf(stderr,"my_cart_rank: %d my_coords[0]:%d [1]:%d\n",my_cart_rank,my_coords[0],my_coords[1]);

//MPI_Dims_create

 
#if 1
  /* Scatter matrix A */
  {
    double **matrix;
    int num_rows,num_cols;


    if(my_rank == ROOT) {
      read_matrix_binary(argv[1],&matrix, &num_rows, &num_cols);
      print_matrix(num_rows,num_cols,matrix,"matrixA");
    }
    

    MPI_Bcast(&num_rows,1,MPI_INT,ROOT,MPI_COMM_WORLD);
    MPI_Bcast(&num_cols,1,MPI_INT,ROOT,MPI_COMM_WORLD);
  
    num_rows_a = num_rows/num_procs_per_dim;
    num_cols_a = num_cols/num_procs_per_dim;

    make_matrix(num_row_a, num_cols_a,&matrix_a);

    
  
  }
#endif
  
 

  
  MPI_Finalize();

  return 0;

}
