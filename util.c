/*
 *Perform read and write of matrixes, and threre allocation and deallocation.
*/
#include <stdio.h>
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#include "debug.h"
#include "util.h"


static inline void error_check_r(size_t retval,size_t elements);
static inline void error_check_w(size_t retval,size_t elements);
static inline void open_file(const char *filename, const char *mode, FILE **fp );


/*
 *Write the matrix to file
*/
void write_matrix_binary(const char* filename, double** matrix,
				int num_rows, int num_cols) {
  assert(filename != NULL);
  assert(matrix   != NULL);
  assert(num_rows > 0);
  assert(num_cols > 0);

  FILE *fp;
 
  open_file(filename,"wb",&fp);

  error_check_w(fwrite (&num_rows, sizeof(int),1, fp),1);
  error_check_w(fwrite (&num_cols, sizeof(int),1, fp),1);
  error_check_w(fwrite (matrix[0], sizeof(double), num_rows*num_cols, fp),num_rows*num_cols);
  
  fclose (fp);
}


/*
 *Reads a matrix from file, allocate memory in a contiuns block
*/
void read_matrix_binary(const char* filename, double*** matrix,
			       int* num_rows, int* num_cols) {
  assert(filename != NULL);
  assert(matrix   != NULL);

  FILE * fp;

  open_file(filename,"rb",&fp);

  /* Read number of rows */
  error_check_r(fread (num_rows, sizeof(int), 1, fp),1);
 
  /* Read number of colums*/
  error_check_r(fread (num_cols, sizeof(int), 1, fp),1);
 /* Storage allocation of the matrix */
  make_matrix(*num_rows,*num_cols,matrix);

  /* Read in the entire matrix */
  error_check_r(fread ((*matrix)[0], sizeof(double), 
		     (*num_rows)*(*num_cols), fp),*num_rows*(*num_cols));

  fclose (fp);
}

/*
* Makes a matrix, with continus memory.
*/
void make_matrix(int num_rows, int num_cols, double *** matrix) {
  assert(num_rows >0);
  assert(num_cols >0);
  assert(matrix != NULL);

  int i; /* loop var*/
  double **m;
  
  if((m = (double **)malloc(num_rows*sizeof(double*)))==NULL) {
    perror("malloc");
    exit(EXIT_FAILURE);
  }
  if((*m = (double *)calloc(num_rows*num_cols,sizeof(double))) == NULL) {
    perror("calloc");
    exit(EXIT_FAILURE);
  }

  /* Build the matrix */
  for (i=1; i<num_rows; i++)
    m[i] = m[i-1]+num_cols;

  /* Return the matrix */
  *matrix = m;
}

/*
 *Free memory alloced in read_matrix_binaryform()
*/
void free_matrix(double **matrix) {
  assert(matrix  != NULL);
  assert(*matrix != NULL);
  
  free(*matrix);
  free(matrix);
}

void* safe_malloc(size_t size) {
  assert(size > 0);
  void * ptr;

  ptr = malloc(size);
  if(ptr == NULL) {
    perror("malloc");
    exit(EXIT_FAILURE);
  }
  return ptr;
}


static inline void error_check_r(size_t retval,size_t elements) {
  
  if(retval == elements) {
    return;
  }else if(retval == -1) {
    perror("fread");
    exit(EXIT_FAILURE);
  }else {
    fprintf(stderr,"fread, wrong number read: %zu\n",retval);
  }
}

static inline void error_check_w(size_t retval,size_t elements) {
  
  if(retval == elements) {
    return;
  }else if(retval == -1) {
    perror("fwrite");
    exit(EXIT_FAILURE);
  }else {
    fprintf(stderr,"fwrite, wrong number writen: %zu\n",retval);
  }
}


static inline void open_file(const char *filename,const char *mode, FILE **fp) {
  if((*fp = fopen (filename,mode) ) == NULL ) {
    fprintf(stderr,"File: %s ",filename);
    perror("fopen");
    exit(EXIT_FAILURE);
  }
}



