/*Contains common defs*/
#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/* File name for input matrix a and b and output matrix c */
#define NUMBER_OF_ARGS  4



typedef struct{
  double **m; /*the matrix*/
  int this_num_rows,this_num_cols; /* locka nr of rows/cols in the matrix*/
  int full_num_rows,full_num_cols; /* total rows/cols of the matrix*/
  char *filename; /* infile or outfile*/
}matrix_t;


inline void assert_arg_number(int argc) {
  if(argc != NUMBER_OF_ARGS) {
    fprintf(stderr,"Wrong number of args: Given: %d expected: %d \n",argc,NUMBER_OF_ARGS);
    exit(EXIT_FAILURE);
  }
}

inline void assert_product_dim(bool equal) {
  if(equal) return;
  fprintf(stderr,"col_dim != row_dim\n");
  exit(EXIT_FAILURE); 
}

/* Calculate number of rows for given MPI process*/
static inline int get_rows_per_process(int num_rows, int num_procs,int my_rank) {
  int retval,extra;
  retval = num_rows/num_procs; 
  extra =  num_rows%num_procs > my_rank;
 
  return retval + (int)extra; /* so the compiler don't do somthing funny*/
}


#endif

