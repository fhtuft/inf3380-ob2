/*
 * Program that perform  matrix multiplication using 
 * mpi with row-wise 1D block partitioning.
*/
#include <stdio.h>
#include <mpi.h>
#include <stdlib.h>
#include <stdbool.h>

#include "debug.h"
#include "util.h"
#include "common.h"

/* Root node in  MPI_COMM_WORLD*/
#define ROOT         0


/* Dimensions of the matrixes */
static int num_rows_global_a;
static int num_cols_global_a;
static int num_rows_global_b;
static int num_cols_global_b;
static int num_rows_global_c;
static int num_cols_global_c;


int main(int argc, char *argv[]) {

  int i,j,k;/* loop variabls */

  /* matrix A */
  double **matrix_a;
  int num_rows_a, num_cols_a;
  /* matrix B */
  double **matrix_b;
  int num_rows_b, num_cols_b;
  /* matrix C */
  double **matrix_c;
  int num_rows_c, num_cols_c;

  /* MPI*/
  int my_rank, num_procs;


  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size(MPI_COMM_WORLD, &num_procs);

  assert_arg_number(argc);

  
  #if 1
  /* 
   * Scatter matrix A 
   */
  {
    double **matrix;
    int num_rows,num_cols;
   
    /* Root reads the matrix from file*/
    if(my_rank == ROOT) 
      read_matrix_binary(argv[1],&matrix, &num_rows, &num_cols);
    
    /* Broadcast the matrix dimensions to all nodes*/
    MPI_Bcast(&num_rows,1,MPI_INT,ROOT,MPI_COMM_WORLD);
    MPI_Bcast(&num_cols,1,MPI_INT,ROOT,MPI_COMM_WORLD);
    
    /* Set lockal dimensions */
    num_rows_a = num_rows/num_procs +(num_rows%num_procs>my_rank);
    num_cols_a = num_cols;
    num_rows_c = num_rows_a;
    num_rows_global_a = num_rows;
    num_cols_global_a = num_cols;
    num_rows_global_c = num_rows_global_a;
    
    make_matrix(num_rows_a, num_cols_a,&matrix_a);
       
    int *sendcount      = (int *)safe_malloc(num_procs*sizeof(int));
    int *displs         = (int *)safe_malloc(num_procs*sizeof(int));
    int recvcount       = num_rows_a*num_cols_a;
   
    /* Make sencount and displs */
    int offset = 0;
    for (i=0; i<num_procs; i++) {
      sendcount[i] =(num_rows/num_procs +(num_rows%num_procs>i))*num_cols_a;
      displs[i] = offset;
      offset += sendcount[i];
    }
   
    /* Scatter matrix A */
    void *sendbuf = (my_rank == ROOT)? &(matrix[0][0]):NULL;
    MPI_Scatterv(sendbuf,sendcount,displs,
		 MPI_DOUBLE,&(matrix_a[0][0]),recvcount,
		 MPI_DOUBLE,
		 ROOT,MPI_COMM_WORLD);
    
    free(sendcount);
    free(displs);

    if(my_rank == ROOT)
      free_matrix(matrix);  
  }
#endif
  
  /* 
   * Scatter matrix B 
  */  
  #if 1
    {

      double **matrix;
      int num_rows,num_cols;

      /* Root reads matrix from file*/
      if(my_rank == ROOT) 
	read_matrix_binary(argv[2],&matrix, &num_rows, &num_cols);

      /* Broadcast dimensions to all nodes*/
      MPI_Bcast(&num_rows,1,MPI_INT,ROOT,MPI_COMM_WORLD);
      MPI_Bcast(&num_cols,1,MPI_INT,ROOT,MPI_COMM_WORLD);
  
      /* Set local dimensions*/
      num_rows_b = (num_rows/num_procs) +(num_rows%num_procs>my_rank);
      num_cols_b = num_cols;
      num_cols_c = num_cols_b;
      num_cols_global_b = num_cols;
      num_rows_global_b = num_rows;
      num_cols_global_c = num_cols_global_b;

      make_matrix(num_rows_b, num_cols_b,&matrix_b);
     
      int *sendcount     = (int *)safe_malloc(num_procs*sizeof(int));
      int *displs        = (int *)safe_malloc(num_procs*sizeof(int));
      int recvcount      = num_rows_b*num_cols_b;
      /* Make sendcout and displs */
      int offset = 0;
      for (i=0; i<num_procs; i++) {
	sendcount[i] =((num_rows/num_procs) +(num_rows%num_procs>i))*num_cols_b;
	displs[i] = offset;
	offset += sendcount[i];
      }
      
      /* Scatter matrix B*/
      void* sendbuf = (my_rank == ROOT)? &(matrix[0][0]):NULL;
      MPI_Scatterv(sendbuf,sendcount,displs,
		   MPI_DOUBLE,&(matrix_b[0][0]),recvcount,
		   MPI_DOUBLE,
		   ROOT,MPI_COMM_WORLD);
    
      free(sendcount);
      free(displs);
    
      if(my_rank == ROOT)
	free_matrix(matrix);
    }
#endif


#if 1  
     /* Make matrix C */

    assert_product_dim(num_cols_global_a  == num_rows_global_b);    
    make_matrix(num_rows_c, num_cols_c,&matrix_c);
  
#endif
 
  
#if 1
  {

    int sendcount;
    int *recvcount;
    int *displs;
    double *send_array;
    double *b_col_array;
    
    recvcount = (int *)safe_malloc(num_procs*sizeof(int));
    displs    = (int *)safe_malloc(num_procs*sizeof(int));
     /* Current lockal part of column in matrix B*/
    send_array = (double*)safe_malloc(num_rows_b*sizeof(double));
    /* Complete current column in matrix B*/
    b_col_array = (double*)safe_malloc(num_cols_a*sizeof(double));
    sendcount = num_rows_b;

    /* Make recvcount and displs */
    int offset = 0;
    for(i =0; i<num_procs; i++) {
      recvcount[i] = ((num_rows_global_b/num_procs) + (num_rows_global_b%num_procs > i));
      displs[i]    = offset;
      offset      += recvcount[i];
    }
    
    /* A*B=C */
    for(j = 0; j<num_cols_c; j++) {
      /* Make send_array */
      for(k=0; k<num_rows_b; k++) {
	send_array[k] = matrix_b[k][j];
      }
      /* Get the j'th column from the other nodes in the communicator */
      MPI_Allgatherv(&(send_array[0]),sendcount,MPI_DOUBLE,
		     &(b_col_array[0]),&(recvcount[0]),&(displs[0])
		     ,MPI_DOUBLE, MPI_COMM_WORLD);
    
#pragma omp parallel for private(k)
      for(i =0; i<num_rows_c ; i++) { 
	/* Vector inner product */
	for(k = 0; k< num_cols_a; k++) {
	  matrix_c[i][j] += matrix_a[i][k]*b_col_array[k];
	}
      }
    }

    free(recvcount);
    free(displs);
    free(send_array);
    free(b_col_array);
  }
#endif

#if 1
  {
    /* Gather the product matrix to ROOT, writes to file */
    
    int i;
    double **matrix;
    int sendcount;
    int *recvcounts;
    int *displs;

    sendcount = num_rows_c*num_cols_c;
    
    recvcounts = (int *)safe_malloc(num_procs*sizeof(int));
    displs    = (int *)safe_malloc(num_procs*sizeof(int));
    /* Make recvcoutn and displs*/
    int offset =0;
    for (i=0; i<num_procs; i++) {
	recvcounts[i] =((num_rows_global_c/num_procs) +(num_rows_global_c%num_procs>i))*num_cols_global_c;	
	displs[i] = offset;
	offset += recvcounts[i];
    }

    if(my_rank == ROOT) 
      make_matrix(num_rows_global_c,num_cols_global_c,&matrix);
    
    /*  Gather the product matrix from the other nodes to ROOT */
    void *recvbuf = (void *)((my_rank == ROOT)? &(matrix[0][0]):NULL);
    MPI_Gatherv(&(matrix_c[0][0]),sendcount, MPI_DOUBLE,
		recvbuf,recvcounts,displs,
                MPI_DOUBLE,ROOT, MPI_COMM_WORLD);
    
    free(recvcounts);
    free(displs);

    if(my_rank == ROOT) {
      /* Write the product matrix to file */
      write_matrix_binary(argv[3],matrix,num_rows_global_c,num_cols_global_c);
      free_matrix(matrix);
    }
    
  }
#endif

  /* Free alloced memory */
  free_matrix(matrix_a);
  free_matrix(matrix_b);
  free_matrix(matrix_c);

  MPI_Finalize();

  return 0; /* Makes the compiler happy  */
}
