/*
 *Program that perform  matrix multiplication A*B=C
*/
#include <stdio.h>
#include <stdlib.h>

#include "debug.h"
#include "util.h"
#include "common.h"

int main(int argc, char *argv[]) {

  int i,j,k;/* loop variabls */
  
  /*A matrix*/
  double **matrix_a;
  int num_rows_a, num_cols_a;
  /*B matrix */
  double **matrix_b;
  int num_rows_b, num_cols_b;
  /* C matrix */
  double **matrix_c;
  int num_rows_c, num_cols_c;

  /* Get the file names from command line */
  assert_arg_number(argc);
  const char *infile_matrix_a  = argv[1]; /*file name*/
  const char *infile_matrix_b  = argv[2]; /*file name*/
  const char *outfile_matrix_c = argv[3]; /*file name*/


  /* Read matrix A */
  read_matrix_binary(infile_matrix_a,&matrix_a, &num_rows_a, &num_cols_a);
  /* Read matrix B */
  read_matrix_binary(infile_matrix_b,&matrix_b, &num_rows_b, &num_cols_b);
  /* Assert correct dimensions in the factors */
  assert_product_dim(num_cols_a == num_rows_b);
  /* Set number of cols and rows in product matrix C */
  num_rows_c = num_rows_a; num_cols_c = num_cols_b;
  /* Make matrix C */
  make_matrix(num_rows_c, num_cols_c,&matrix_c);

  /* A*B=C */
  for(i =0; i<num_rows_c ; i++) {
    for(j = 0; j<num_cols_c; j++) {
      /* Vector inner product */
      for(k = 0; k< num_cols_a; k++) {
	matrix_c[i][j] += matrix_a[i][k]*matrix_b[k][j];
      }
    }
  }

  /* Write the product matrix to file */
  write_matrix_binary(outfile_matrix_c,matrix_c,num_rows_c,num_cols_c);

  
  /* Free alloced memory */
  free_matrix(matrix_a);
  free_matrix(matrix_b);
  free_matrix(matrix_c);

  exit(EXIT_SUCCESS);
}
