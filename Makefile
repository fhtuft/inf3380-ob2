CC = mpicc
CFLAG =  -O3 -Wall -fopenmp  #-Wextra # -g 


LINKER = mpicc -o
LFLAG = -lm -Wall

MPI_NUM_PROCS = 4
MPIRUN = mpirun -np $(MPI_NUM_PROCS)

binaries = mm_dim1 mm_serial 
SRC = util.c mm_dim1.c mm_serial.c 

RM = rm -f

DATA   = data/
OUTPUT = output/
DEL_OUTPUT = $(RM) $(OUTPUT)*.bin 

#Devilry
USER_NAME    = fhtuft
COURSE       = inf3380
OBNR         = ob2
DEVILRY_PATH = ../$(USER_NAME)/
DEVILRY_FILE = $(OBNR)_$(COURSE)_$(USER_NAME).tar

#Command line parameters for running the code
small_matrix_in     = data/small_matrix_a.bin data/small_matrix_b.bin 
large_matrix_in     = data/large_matrix_a.bin data/large_matrix_b.bin
small_matrix_out    = output/small_matrix.bin
large_matrix_out    = output/large_matrix.bin
small_matrix_test   = test/small_matrix_c.bin
large_matrix_test   = test/large_matrix_c.bin
serial_small_args   = $(small_matrix_in)  $(small_matrix_out)  
serial_large_args   = $(large_matrix_in)  $(large_matrix_out)
parallel_small_args = $(small_matrix_in)  $(small_matrix_out)
parallel_large_args = $(large_matrix_in)  $(large_matrix_out)

#For testing
DIFF = diff -q  #report only difference

.PHONY: all
all: $(binaries)

%.o: %.c
	$(CC) $< $(CFLAG) -c -o $@

.PHONY: debug
debug: CC += -DDEBUG
debug: all


mm_cannon: mm_cannon.o util.o
	$(CC) -fopenmp $^ $(LFLAG) -o $@ 

mm_dim1: mm_dim1.o util.o
	$(CC) -fopenmp $^ $(LFLAG) -o $@ 

mm_serial: mm_serial.o util.o
	$(CC) $^ $(LFLAG) -o $@ 



.PHONY: clean
clean:
	$(RM) $(binaries) 
	$(RM) *~
	$(RM) *.o
	$(RM) $(DEVILRY_FILE)
	$(DEL_OUTPUT)


.PHONY: test
test: clean all	
#./mm_serial $(serial_small_args)
#$(DIFF) $(small_matrix_out) $(small_matrix_test) 
#$(DEL_OUTPUT)
#./mm_serial $(serial_large_args)
#$(DIFF) $(large_matrix_out) $(large_matrix_test) 
	$(DEL_OUTPUT)
	$(MPIRUN) ./mm_dim1 $(parallel_small_args)
	$(DIFF) $(small_matrix_out) $(small_matrix_test) 
	./mm_dim1 $(parallel_large_args)
	$(DIFF) $(large_matrix_out) $(large_matrix_test) 

.PHONY: test_dim1
test_dim1: clean mm_dim1
	$(DEL_OUTPUT)
	$(MPIRUN) ./mm_dim1 $(parallel_small_args)
	$(DIFF) $(small_matrix_out) $(small_matrix_test) 

.PHONY: test_cannon
test_cannon: clean mm_cannon
	$(DEL_OUTPUT)
	$(MPIRUN) ./mm_cannon $(parallel_small_args)
	$(DIFF) $(small_matrix_out) $(small_matrix_test) 

.PHONY: devilry
devilry: clean
	mkdir -p ../$(USER_NAME)
	rm -fr $(DEVILRY_PATH)*
	cp -r * $(DEVILRY_PATH)
	tar -cvf $(DEVILRY_FILE) $(DEVILRY_PATH)*
