/*
 * Functions to read write and handel memory
*/
#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>
#include <assert.h>


void* safe_malloc(size_t size); 

void make_matrix(int num_rows, int num_cols, double *** matrix);

void free_matrix(double **matrix);

void read_matrix_binary(const char* filename, double*** matrix,
			       int* num_rows, int* num_cols);

void write_matrix_binary(const char* filename, double** matrix,
				int num_rows, int num_cols);
#endif
